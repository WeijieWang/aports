# Contributor: Andrej Kolchin <KAAtheWise@protonmail.com>
# Maintainer: Andrej Kolchin <KAAtheWise@protonmail.com>
pkgname=vale
pkgver=3.5.0
pkgrel=1
pkgdesc="A markup-aware linter for prose built with speed and extensibility in mind"
url="https://vale.sh/"
arch="all"
license="MIT"
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/errata-ai/vale/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -ldflags "-X main.version=v$pkgver" -o bin/vale ./cmd/vale
}

check() {
	go test ./internal/core ./internal/lint ./internal/check ./internal/nlp ./internal/glob ./cmd/vale
}

package() {
	install -Dm755 bin/vale -t "$pkgdir"/usr/bin

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
131f102033090e81afabf883068f96d30c81b4dd24fac51e86fa71ce76a81b9da23063837e3cee2464394f6af5f5ef2153ef1effceb3ca1554b04639b8aad109  vale-3.5.0.tar.gz
"
