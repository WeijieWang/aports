# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=janet
pkgver=1.34.0
pkgrel=0
pkgdesc="Dynamic Lisp dialect and bytecode VM"
url="https://janet-lang.org/"
license="MIT"
arch="all"
makedepends="meson"
subpackages="$pkgname-static $pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/janet-lang/janet/archive/v$pkgver.tar.gz
	loongarch64.patch
	"

# secfixes:
#   1.22.0-r0:
#     - CVE-2022-30763

case "$CARCH" in
x86_64) ;;
# FIXME
*) options="$options !check" ;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Depoll=true \
		-Dos_name="alpine" \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -dm755 "$pkgdir"/usr/share/doc/janet
	cp -a examples "$pkgdir"/usr/share/doc/janet
}

sha512sums="
9806044c738bd142e39e1fd8cdf696a8ae6094cfdefbfa5fe93df4c3c3e52387ab43ba4417054f67db94b138c3618f358fac73a9065ce86b3fcb6d415e2a8f2e  janet-1.34.0.tar.gz
ff491f900edc37318e0bceb87826cf1d311979b801a9d595dcf2d8eca5bf6e11652dbb538ed50bac4a52fd08cec1eff0eb5887ef8cb5020d7d855baaf564044b  loongarch64.patch
"
