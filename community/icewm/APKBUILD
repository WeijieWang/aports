# Contributor: Paul Bredbury <brebs@sent.com>
# Contributor: gay <gay@disroot.org>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=icewm
pkgver=3.5.1
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
license="LGPL-2.0-only"
makedepends="
	alsa-lib-dev
	cmake
	fribidi-dev
	glib-dev
	imlib2-dev
	libao-dev
	libintl
	librsvg-dev
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	markdown
	perl
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"
options="!check" # No test suite

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc/icewm \
		-DENABLE_NLS=OFF \
		-DCONFIG_IMLIB2=ON \
		-DCONFIG_LIBRSVG=ON \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
984a1debb4cba64e38b091f551aa7542ed0e3572d4a1f994dc9cf44730e277b28d1b8228df4c0d12b6c55abf71463d678624bf3bf7b750929a58243c4e82a8cd  icewm-3.5.1.tar.lz
"
